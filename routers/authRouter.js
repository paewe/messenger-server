const express = require("express");
const router = express.Router();
const validateForm = require("../controllers/validateForm");
const pool = require("../db");
const bcrypt = require("bcrypt");

router
    .route("/login")
    .get( async (req, res) => {
        if (req.session.user && req.session.user.username) {
            console.log("logged in");
            res.json({ loggedIn: true, username: req.session.user.username });
        } else {
            res.json({ loggedIn: false });
        }
    })
    .post( async (req, res) => {
    validateForm(req, res);
    console.log(req.session);

    const potencialLogin = await pool.query("SELECT id, username, passhash FROM users u WHERE u.username=$1", [req.body.username])
    if (potencialLogin.rowCount > 0) {
        const isSamePass = bcrypt.compare(
            req.body.password,
            potencialLogin.rows[0].passhash
        );
        if (isSamePass) {
            // login
            req.session.user = {
                username: req.body.username,
                id: potencialLogin.rows[0].id
            };
            res.json({ loggedIn: true, username: req.body.username })
        } else {
            // not good login
            console.log("not good");
            res.json({ loggedIn: false, status: "Wrong username or password!" })
        }
    } else {
        console.log("not good");
        res.json({ loggedIn: false, status: "Worng username or password!" })
    }
});

router.post("/signup", async (req, res) => {
    validateForm(req, res);

    const existingUser = await pool.query("SELECT username from users WHERE username=$1", [req.body.username]);
    console.log(existingUser);

    if (existingUser.rowCount === 0) {
        // register
        const hashedPass = await bcrypt.hash(req.body.password, 10);
        const newUserQuery = await pool.query(
            "INSERT INTO users(username, passhash) values ($1, $2) RETURNING id, username",
            [req.body.username, hashedPass]
        );
        console.log(newUserQuery);
        req.session.user = {
            username: req.body.username,
            id: newUserQuery.rows[0].id
        };
        res.json({ loggedIn: true, username: req.body.username })
    } else {
        res.json({loggedIn: false, status: "Username taken"});
    }
});

module.exports = router;
